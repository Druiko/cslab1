﻿using System;

namespace cslab1
{
    internal class Program
    {
        private static void Main(string[] args)
        {
            Console.Write("Введите число N: ");
            int N;
            N = int.Parse(Console.ReadLine());
            int numcount = NumCount(N);
            Console.WriteLine("1) Количество цифр в числе " + N + " равно " + numcount);
            Console.Write("2) Массив из цифр в числе: "); Massive(N);
            Console.WriteLine("");
            int average = Average(N);
            Console.WriteLine("3) Среднее арифметическое цифр в числе: " + average);
            int geometric = Geometric(N);
            Console.WriteLine("4) Среднее геометрическое цифр в числе: " + geometric);
            int factorial = Factorial(N);
            Console.WriteLine("5) Факториал числа: " + factorial);
            int paired = Paired(N);
            Console.WriteLine("6) Сумма парных чисел от 1 до " + N + ":" + paired);
            int unpaired = Unpaired(N);
            Console.WriteLine("7) Сумма непарных чисел от 1 до " + N + ":" + unpaired);
        }

        private static int NumCount(int N)
        {
            return Math.Abs(N).ToString().Length;
        }

        private static void Massive(int N)
        {
            int temp = N;
            int count = 0;
            while (temp > 0)
            {
                count++;
                temp = temp / 10;
            }
            int[] mas = new int[count];
            int j = count - 1;
            while (N > 0)
            {
                mas[j--] = N % 10;
                N = N / 10;
            }
            for (int i = 0; i < count; i++)
            {
                Console.Write(mas[i] + " ");
            }
        }

        private static int Average(int N)
        {
            int res = 0;
            int count = 0;
            while (N != 0)
            {
                res = res + N % 10;
                N = N / 10;
                count++;
            }
            return res / count;
        }

        private static int Geometric(int N)
        {
            int res = 0;
            int count = 0;
            while (N != 0)
            {
                res = res + N % 10;
                N = N / 10;
                count++;
            }
            return res / count;
        }

        private static int Factorial(int N)
        {
            int factorial = 1;
            if (N != 0)
            {
                for (int i = 2; i <= N; i++)
                {
                    factorial *= i;
                }
            }
            return factorial;
        }

        private static int Paired(int N)
        {
            int sum = 0;
            for (int i = 0; i <= N; i++)
            {
                if (i % 2 == 0)
                {
                    sum = sum + i;
                }
            }
            return sum;
        }

        private static int Unpaired(int N)
        {
            int sum = 0;
            for (int i = 1; i <= N; i++)
            {
                if (i % 2 != 0)
                {
                    sum = sum + i;
                }
            }
            return sum;
        }
    }
}